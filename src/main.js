import Vue from 'vue'
import App from './App.vue'
import router from './router';
import Buefy from 'buefy';
import store from './store';

import VueYoutube from 'vue-youtube'

// Require the main Sass manifest file
// require('@/assets/scss/main.scss');

// Buefy
Vue.use(Buefy);

Vue.use(VueYoutube)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
