import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    rounds: [],
    currentRound: null,
    currentTeam: null,
    currentTeamIndex: null,
    teams: []
  },
  getters:{
    rounds: state => {
      return state.rounds;
    },
    currentRound: state => {
      return state.currentRound;
    },
    currentTeam: state => {
      return state.currentTeam;
    },
    currentTeamIndex: state => {
      return state.currentTeamIndex;
    },
    teams: state => {
      return state.teams;
    }
  },
  mutations: {
    rounds(state, payload) {
      state.rounds = payload;
    },
    currentRound(state, payload) {
      state.currentRound = payload;
    },
    currentTeam(state, payload) {
      state.currentTeam = payload;
    },
    currentTeamIndex(state, payload) {
      state.currentTeamIndex = payload;
    },
    teams(state, payload) {
      state.teams = payload;
    }
  },
  actions: {

  } 
});

export default store;