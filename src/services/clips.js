import axios from 'axios';

export default new class {

  loadClips() {
    return axios({
      baseURL: '',
      method: 'GET',
      url: 'data/movieclips.json',
      responseType: 'json'
  })
  .then((response) => response.data);
  }
}