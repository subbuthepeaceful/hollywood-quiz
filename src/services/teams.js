import axios from 'axios';

export default new class {

  loadTeams() {
    return axios({
      baseURL: '',
      method: 'GET',
      url: 'data/teams.json',
      responseType: 'json'
  })
  .then((response) => response.data);
  }
}