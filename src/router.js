import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Home'
import VideoQuestion from '@/views/VideoQuestion'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/question',
      name: 'question',
      component: VideoQuestion
    }
  ]
})

export default router;
